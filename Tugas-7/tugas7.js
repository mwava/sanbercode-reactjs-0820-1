class Animal{
    constructor(name) {
        this.name=name;
        this._legs=4;
        this._cold_blooded=false;
    }
}

class Frog extends Animal{
    constructor(name,_cold_blooded){
        super(name);
    }
    get cold_blooded(){
        return this._cold_blooded;
    }
    set cold_blooded(z){
        this._cold_blooded=z
    }
    jump(){
        return "hop hop";
    }
}
class Ape extends Animal{
    constructor(name,_cold_blooded,_legs){
        super(name,_cold_blooded);
    }
    get legs(){
        return this._legs;
    }
    set legs(l){
        this._legs=l
    }
    yell(){
        return "Auooo";
    }
}

let kera =new Ape("Kera");
kera.legs =2;

let katak = new Frog("Katak");
katak.cold_blooded=true;

sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep._legs);
console.log(sheep._cold_blooded);
console.log();
//ape
console.log(kera.name);
console.log(kera.legs);
console.log(kera._cold_blooded);
console.log(kera.yell());
console.log();

//frog
console.log(katak.name);
console.log(katak._legs);
console.log(katak.cold_blooded);
console.log(katak.jump());
console.log();

//soal2
console.log("Time Lapse")
class Clock{
    constructor({template}){
        this.template=template;
        this.timer=null;
    }
    render(){
        let date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
    stop() {
        clearInterval(this.timer);
      };
    
      start (){
        this.timer = setInterval(this.render.bind(this), 1000);
      };
}

  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
