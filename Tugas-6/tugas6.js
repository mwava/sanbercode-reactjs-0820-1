console.log("\n")
//soal1
const x = 4;

let luasLingkaran =(jariPertama)=>{
    return 3.14*jariPertama*jariPertama;
}
let kelilingLingkaran=(jariPertama)=>{
  return 2*3.14*jariPertama
}
console.log(`Luas lingkaran ialah ${luasLingkaran(x)}`);
console.log(`keliling lingkaran ialah ${kelilingLingkaran(x)}`);
console.log("\n");

//soal2
let kalimat ="";
const penambahan = ()=>{
    const pertama ="saya";
    const kedua ="adalah";
    const ketiga ="seorang";
    const keempat ="frontend";
    const kelima ="developer";
    kalimat+=`${pertama} ${kedua} ${ketiga} ${keempat} ${kelima}`;
    console.log(kalimat)
    return;
}
penambahan();
console.log("\n");


//soal3
const newFunction =(firstName, lastName)=>{
    return {
      firstName,
      lastName,
      fullName:()=>{
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName(); 
  console.log("\n");

  //soal4
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const{firstName,lastName,destination,occupation,spell}=newObject;
  console.log(firstName,lastName,destination,occupation,spell);
  console.log("\n");

  //soal5
  const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined);