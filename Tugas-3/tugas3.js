//soal 1
var kataPertama ="saya";
var kataKedua ="senang";
var kataKetiga ="belajar";
var kataKeempat ="javascript";
console.log(kataPertama + " " + kataKedua.charAt(0).toUpperCase() + kataKedua.substring(1)+ " " + kataKetiga + " " + kataKeempat.toUpperCase());
console.log("\n")

//soal 2
var kataPertama ="1";
var kataKedua ="2";
var kataKetiga ="4";
var kataKeempat ="5";
console.log(parseInt(kataPertama)+parseInt(kataKedua)+parseInt(kataKetiga)+parseInt(kataKeempat));
console.log("\n")

//soal3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10)
var kataKetiga = kalimat.substring(15,18)
var kataKeempat = kalimat.substr(19,5)
var kataKelima = kalimat.substring(25,31) 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log("\n")

//soal4
var nilai=65;
if(nilai >= 80){
    console.log("A");
}else if(nilai >= 70){
    console.log("B");
}else if(nilai >= 60){
    console.log("C");
}else if(nilai >= 50){
    console.log("D");
}else if(nilai < 50){
    console.log("E");
}
console.log("\n")

//soal5
var tanggal = 12;
var bulan = 9;
var tahun = 2001;
switch (bulan =9){
    case 1:  {bulan="January"; break;}
    case 2:  {bulan="February"; break;}
    case 3:  {bulan="March"; break;}
    case 4:  {bulan="April"; break;}
    case 5:  {bulan="Mei"; break;}
    case 6:  {bulan="June"; break;}
    case 7:  {bulan="July"; break;}
    case 8:  {bulan="August"; break;}
    case 9:  {bulan="September"; break;}
    case 10: {bulan="October"; break;}
    case 11: {bulan="November"; break;}
    case 12: {bulan="December"; break;}
    default: {bulan="Uknown Months"; break;}
}
console.log(tanggal.toString()+ " " + bulan + " " + tahun.toString());
