//soal1
var kataPertama="I love coding";
var kataKedua="I will become a frontend developer";
var n =2;
//Looping pertama
console.log("looping pertama \n".toUpperCase());
while(n<=20 ){
    console.log(n+" - "+kataPertama);
    n +=2;
}console.log("\n");

var n =20;
//Looping kedua
console.log("looping kedua \n".toUpperCase());
while(n>=2){
    console.log(n+" - "+kataKedua);
    n-=2;
}
console.log("\n")

//soal2
for(var x =1;x<=20;x++){
    if(x % 1 ==0 && x%3 == 0 && x%2 !=0 ){
        console.log(x+" - I Love Coding");
    }else if(x % 2 ==0){
        console.log(x+" - Bekualitas");
    }else if(x % 1==0){
        console.log(x+" - Santai");
    }
}

console.log("\n");

//soal3
for(var y =1;y<=7;y++){
   console.log('*'.repeat(y));
}
console.log("\n");

//soal4
var kalimat ="saya sangat senang belajar javascript";
kalimat=kalimat.split(" ")
console.log(kalimat);
console.log("\n");

//soal5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
for(var i=0;i<5;i++){
    console.log(daftarBuah[i]);
}